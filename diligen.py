"""
Class to make async requests for the diligen
"""
import argparse
import os
import logging

import asyncio
import aiohttp

from slugify import slugify


logging.basicConfig(format='%(levelname)s (%(asctime)s) : %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


class Diligen:
    """
    Diligen class. Make requests to the api and save results
    in the defined folder. Errors will be saved in the sub-folder
    See variable _errors_folder
    """

    # Settings from the init
    _input_path = None
    _output_folder_path = None

    _client_id = None
    _auth_token = None
    # Eof settings from the input

    _errors_folder = 'errors'  # sub-folder to save errors from the api

    analyze_url = 'https://kp2018.api.diligen.com/analyze'
    extract_url = 'https://api.diligen.com/extract'

    _headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }

    max_chunk_size = 25
    requests_limit = 50
    max_retry = 10
    _semaphore = None

    def __init__(self, input_path: str, output_folder_path: str, auth_token: str, client_id: str, loop):
        """
        Keep arguments from the call

        :param input_path: path to the file with urls
        :param output_folder_path: path to the folder where to save results
        :param auth_token: token for the api
        :param client_id: client id for the api
        :param loop: event loop
        """
        self.input_path = input_path
        self.output_folder_path = output_folder_path
        self.auth_token = auth_token
        self.client_id = client_id
        self.loop = loop

    @property
    def semaphore(self):
        """
        Instance of semaphore
        :return:
        """
        if self._semaphore is None:
            self._semaphore = asyncio.Semaphore(self.requests_limit)
        return self._semaphore

    @property
    def errors_path(self):
        """
        Property with an errors folder path

        :return: path to the errors folder
        """
        path = os.path.join(
            self.output_folder_path, self._errors_folder
        )
        if not os.path.exists(path):
            os.makedirs(path)
        return path

    @property
    def input_path(self):
        """
        :return: path to the file with the urls
        """
        return self._input_path

    @property
    def headers(self):
        """
        :return: dict with the headers
        """
        if ('auth_token' not in self._headers
                or 'client_id' not in self._headers):
            self._headers['auth_token'] = self.auth_token
            self._headers['client_id'] = self.client_id
        return self._headers

    @input_path.setter
    def input_path(self, path):
        """
        Setter for the input file path.
        Raise an exception if file doesn't exist
        :param path: input file path.
        :return:
        """
        assert os.path.exists(path), "Input path doesn't exist"
        self._input_path = path

    @property
    def output_folder_path(self):
        """
        :return: path to the folder with results
        """
        return self._output_folder_path

    @output_folder_path.setter
    def output_folder_path(self, path):
        """
        Setter for the output folder.
        Create directory if it doesn't exist
        :param path:
        :return:
        """
        if not os.path.exists(path):
            os.makedirs(path)
        self._output_folder_path = path

    @property
    def auth_token(self):
        """
        Auth token property
        :return:
        """
        return self._auth_token

    @auth_token.setter
    def auth_token(self, token):
        """
        Token setter. Raise an exception
        if token doesn't exist
        :param token:
        :return:
        """
        assert token, 'Wrong token'
        self._auth_token = token

    @property
    def client_id(self):
        """
        Client id getter
        :return:
        """
        return self._client_id

    @client_id.setter
    def client_id(self, value):
        """
        Client id setter. raise an exception
        if client id doesn't exist
        :param value:
        :return:
        """
        assert value, 'Wrong client id'
        self._client_id = value

    async def run(self):
        """
        Entry point of the class.
        Run function run_queries
        :return:
        """
        await self.run_queries()

    async def run_queries(self):
        """
        Start logic of the current class.
        It read a file with the urls and run
        coroutines with requests

        :return:
        """

        # open session
        async with aiohttp.ClientSession(loop=self.loop) as session:
            # read file
            with open(self.input_path) as data:
                routines = []
                counter = 0
                chunks = 0
                for file_url in data:
                    counter += 1
                    # collect routines
                    routines.append(self.extract(session, {"path": file_url.strip()}))
                    if len(routines) >= self.max_chunk_size:
                        chunks += 1
                        logger.info('started chunk: %s', chunks)
                        # run routines by batches
                        await asyncio.gather(*routines)
                        logger.info(
                            'executed chunk: %s (Total link: %s)', chunks, counter
                        )
                        routines.clear()
                # run the rest of coroutines
                if routines:
                    await asyncio.gather(*routines)

    async def extract(self, session, data: dict, retry=True, retry_count=0):
        """
        Run extract request
        :param session: aiosession
        :param data: dict with the url
        :param retry: should function make a retry
        :param retry_count: count of retry for this function
        :return:
        """
        # run request
        try:
            async with self.semaphore, session.post(
                self.extract_url, json=data, headers=self.headers
            ) as resp:
                # await the result
                result = await resp.text()

                if resp.status == 200:
                    # if it's ok run analyze
                    await self.analyze(session, result, data['path'])
                elif resp.status == 504:
                    # retry the request if it's a first time
                    if retry:
                        await self.extract(session, data, False, retry_count)
                    else:
                        # or write an error for the second one
                        self.write_error(result, data['path'])
                else:
                    # write an error
                    self.write_error(result, data['path'])
        except Exception as server_exception:
            logger.info(
                'Exception in extract %s; retry-count %s',
                repr(server_exception), retry_count
            )
            retry_count += 1
            await asyncio.sleep(retry_count)
            if retry_count > self.max_retry:
                self.write_error(repr(server_exception), data['path'])
                return
            await self.extract(session, data, retry_count=retry_count)

    async def analyze(self, session, data, file_name, retry=True, retry_count=0):
        """
        Function to execute analyze request

        :param session: aiohttp session
        :param data: data from the extract request
        :param file_name: original url to file
        :param retry: can function retry
        :param retry_count: number of retry for this function
        :return:
        """
        try:
            async with self.semaphore, session.post(
                self.analyze_url, data=data, headers=self.headers
            ) as resp:
                result = await resp.text()
                if resp.status == 200:
                    # write ok result
                    self.write_result(data, file_name)
                elif resp.status == 504:
                    if retry:
                        # retry for the first time
                        await self.analyze(session, data, file_name, False, retry_count)
                    else:
                        # write an error
                        self.write_error(result, file_name)
                else:
                    # write an error
                    self.write_error(result, file_name)
        except Exception as server_exception:
            logger.info(
                'Exception in analyze %s; retry-count: %s',
                repr(server_exception), retry_count
            )
            retry_count += 1
            await asyncio.sleep(retry_count)
            if retry_count > self.max_retry:
                self.write_error(server_exception, file_name)
                return
            await self.analyze(session, data, file_name, retry_count=retry_count)

    def write_result(self, data, name):
        """
        Function to write good result

        :param data: text from the analyze
        :param name: original url of the file
        :return:
        """

        path = os.path.join(
            self.output_folder_path, slugify(name)
        )

        self._write(path, data)

    def write_error(self, data, name):
        """
        Function to write errors

        :param data: text from the analyze or extract
        :param name: original url of the file
        :return:
        """
        path = os.path.join(
            self.errors_path, slugify(name)
        )

        self._write(path, data)

    def _write(self, path, data):
        """
        Function to write data to file

        :param path: path to the file
        :param data: dict to write in the file
        :return:
        """
        logging.debug(path)
        with open('%s.json' % path, 'w') as file_to_write:
            file_to_write.write(data)


def main():
    """
    Entry point of this file

    :return:
    """

    # collect user input
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "input_file",
        help="Full or relative path to the input file",
        type=str
    )

    parser.add_argument(
        "destination_folder",
        help="a full path to the destination folder",
        type=str
    )

    args = parser.parse_args()
    destination_folder = args.destination_folder
    input_file = args.input_file

    token = os.environ.get('TOKEN')
    client_id = os.environ.get('CLIENT_ID')

    # start event loop and execute a function
    loop = asyncio.get_event_loop()
    try:
        # make a class instance
        diligen = Diligen(
            input_path=input_file, output_folder_path=destination_folder,
            auth_token=token, client_id=client_id, loop=loop
        )
        # run requests in the event loop
        loop.run_until_complete(diligen.run())
    finally:
        loop.close()


if __name__ == '__main__':
    main()
